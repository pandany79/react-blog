import jsonPlaceholder from './../api/jsonPlaceholder'
import _ from 'lodash'

export const fetchPostAndUsers = () => async (dispatch, getState) => {
  await dispatch(fetchPost())

  const userIds = _.uniq(_.map(getState().posts, 'userId'))
  userIds.forEach(id => dispatch(fetchUser(id)))

  // Another way using _.chain
  // _.chain(getState().posts)
  //   .map('userId')
  //   .uniq()
  //   .forEach(id => dispatch(fetchUser(id)))
  //   .value() // .value() function lets _.chain know it needs to be executed.
}

export const fetchPost = () => {
  return async (dispatch) => {
    const posts = (await jsonPlaceholder.get('/posts')).data

    dispatch({
      type: 'FETCH_POSTS',
      payload: posts
    })
  }
}

export const fetchUser = id => async dispatch => {
  const user = (await jsonPlaceholder.get(`/users/${id}`)).data
  
  dispatch({
    type: 'FETCH_USER',
    payload: user
  })
}

// Memoized version
// export const fetchUser = id => dispatch => {
//   _fetchUser(id, dispatch)
// }

// const _fetchUser = _.memoize(async (id, dispatch) => {
//   const user = (await jsonPlaceholder.get(`/users/${id}`)).data

//   dispatch({
//     type: 'FETCH_USER',
//     payload: user
//   })
// })